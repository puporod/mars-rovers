package com.points.marsrovers;

import java.util.UUID;

import com.points.marsrovers.communication.Instruction;
import com.points.marsrovers.navigation.Location;
import com.points.marsrovers.navigation.Plateau;
import com.points.marsrovers.navigation.Position;

public class Spirit implements Rover {
	
	private UUID id = UUID.randomUUID();
	private Location location;
	private Instruction instruction;
	private final Plateau plateau;
	
	private boolean deployedOutsidePlateauBounds;

	public Spirit(Location location, Instruction instruction, Plateau plateau) {

		boolean isInputOk = (location != null && instruction != null && plateau != null);
		if (!isInputOk) {
			throw new IllegalArgumentException("Could not create Spirit rover due to null values passed");
		}
		
		this.location = location;
		this.instruction = instruction;
		this.plateau = plateau;	
		
		setRoverDeployedOutOfPlateauBounds(
				!plateau.isPositionInsideBounds(location.getCurrentPosition()));
	}

	@Override
	public void left() {
		location.turnLeft();
	}

	@Override
	public void right() {
		location.turnRight();
	}

	@Override
	public void move() {
		Position futurePosition = location.getCurrentPosition();
		futurePosition.move(location.getCurrentDirection());
		
		if (!plateau.hasColision(futurePosition)
				&& plateau.isPositionInsideBounds(futurePosition)) {
			
			plateau.unpin(location.getCurrentPosition());
			location.move();
			plateau.pin(location.getCurrentPosition());
		}
	}

	@Override
	public Location current() {
		return location;
	}

	@Override
	public void start() {
		if (!isDeployedOutsidePlateauBounds()) {
			instruction.executeOn(this);	
		}
	}
	
	@Override
	public void snapshot() {
		throw new UnsupportedOperationException("snapshot()"); // TODO implement feature
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public boolean isDeployedOutsidePlateauBounds() {
		return deployedOutsidePlateauBounds;
	}

	public void setRoverDeployedOutOfPlateauBounds(boolean roverDeployedOutOfPlateauBounds) {
		this.deployedOutsidePlateauBounds = roverDeployedOutOfPlateauBounds;
	}
}
