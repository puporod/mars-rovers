package com.points.marsrovers;

import org.springframework.stereotype.Service;

import com.points.marsrovers.navigation.Plateau;

@Service
public class PlateauService {
		
	public Plateau createPlateuFrom(String upperRightPosition) {
		return new Plateau(upperRightPosition);
	}
}
