package com.points.marsrovers.navigation;

public class Direction {
	
	private CardinalDirection cardinalDirection;
	
	public Direction(CardinalDirection cardinalDirection) {
		this.cardinalDirection = cardinalDirection;
	}
	
	public void left() {
		cardinalDirection = cardinalDirection.left();
	}
	
	public void right() {
		cardinalDirection = cardinalDirection.right();
	}
	
	public CardinalDirection get() {
		return cardinalDirection;
	}
	
	public Direction copy() {
		return new Direction(CardinalDirection.getCardinalDirection(cardinalDirection.getId()));
	}
	
	@Override
	public String toString() {
		return cardinalDirection.toString();
	}
}
