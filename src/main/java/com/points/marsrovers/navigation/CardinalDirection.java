package com.points.marsrovers.navigation;

public enum CardinalDirection {
	
	N(0), E(1), S(2), W(3);
	
	private final int id;
	
	CardinalDirection(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public CardinalDirection left() {
		return getCardinalDirection(id - 1);
	}
	
	public CardinalDirection right() {
		return getCardinalDirection(id + 1);
	}
	
	public static CardinalDirection getCardinalDirection(int id) {
		
		CardinalDirection[] cardinalDirections = CardinalDirection.values();
		
		boolean isIdInsideBounds = (id >= 0 && id < cardinalDirections.length);
		
		if (isIdInsideBounds) {
			return cardinalDirections[id];
		
		} else if (id < CardinalDirection.N.getId()) {
			return CardinalDirection.W;
		}
		
		return CardinalDirection.N;
	}
}