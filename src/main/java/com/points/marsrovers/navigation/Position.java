package com.points.marsrovers.navigation;

import java.util.Objects;
import java.util.regex.Pattern;

public class Position {

	public static final String POSITION_REGEX_PATTERN = "^[0-9]{1,9} [0-9]{1,9}$";
	public static final Pattern POSITION_REGEX = Pattern.compile(POSITION_REGEX_PATTERN);
	private long x;
	private long y;
	
	public Position(long x, long y) {
		this.x = x;
		this.y = y;
	}

	public Position(String position) {
		if (isPositionValid(position)) {

			String[] positions = position.split(" ");

			this.x = new Long(positions[0]);
			this.y = new Long(positions[1]);
			
		} else {
			throw new IllegalArgumentException(
				"Position value does not match the regular expression validation");
		}
	}
	
	public boolean equalHigherThan(Position p) {
		return p != null && (this.x >= p.x && this.y >= p.y);
	}
	
	public boolean equalLowerThan(Position p) {
		return p != null && (this.x <= p.x && this.y <= p.y);
	}
	
	public Position copy() {
		return new Position(this.x, this.y);
	}
	
	public void move(Direction direction) {
		switch (direction.get()) {
		case N:
			increaseY();
			break;
		case E:
			increaseX();
			break;
		case S:
			decreaseY();
			break;
		case W:
			decreaseX();
			break;
		default:
			break;
		}
	}
	
	private void increaseX() { x++; }
	private void decreaseX() { x--; }
	private void increaseY() { y++; }
	private void decreaseY() { y--; }
		
	public static boolean isPositionValid(String position) {
		return POSITION_REGEX.matcher(position).matches();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(x);
		sb.append(" ");
		sb.append(y);
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		return x == other.x && y == other.y;
	}
}