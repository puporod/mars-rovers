package com.points.marsrovers.navigation;

import java.util.HashSet;
import java.util.Set;

public class Plateau {
		
	private Set<Position> usedPositions = new HashSet<>();
	private Position leftBottom = new Position(0L, 0L);
	private Position upperRight;
	
	public Plateau(String upperRightPosition) {
		setUpperRight(new Position(upperRightPosition));
	}
	
	public Plateau(Position upperRight) {
		setUpperRight(upperRight);
	}

	private void setUpperRight(Position upperRight) {
		if (!leftBottom.equalLowerThan(upperRight)) {
			throw new IllegalArgumentException(
				"The 'upperRight: " + upperRight 
				+"' Plateau's position value must be greater than the 'leftBottom: " + leftBottom
				+"' value in order to create the Plateau's grid.");
		}
		
		this.upperRight = upperRight;
	}
	
	public boolean pin(Position position) {
		return usedPositions.add(position);
	}
	
	public boolean unpin(Position position) {
		return usedPositions.remove(position);
	}

	public boolean hasColision(Position position) {
		return usedPositions.contains(position);
	}
	
	public boolean isPositionInsideBounds(Position position) {
		return leftBottom.equalLowerThan(position) && upperRight.equalHigherThan(position);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(leftBottom);
		sb.append(" x ");
		sb.append(upperRight);
		return sb.toString();
	}
}
