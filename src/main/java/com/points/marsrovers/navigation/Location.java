package com.points.marsrovers.navigation;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class Location {

	public static final String POSITION_DIRECTION_PATTERN = "^[0-9]{1,9} [0-9]{1,9} (?i)[NESW]$";
	private static final Pattern POSITION_DIRECTION_REGEX = Pattern.compile(POSITION_DIRECTION_PATTERN);
	
	private List<String> locationHistory = new LinkedList<>();
	private Position position;
	private Direction direction;
	
	public Location(String positionAndDirectionToParse) {
		
		boolean isRegexValid = POSITION_DIRECTION_REGEX.matcher(positionAndDirectionToParse).matches();
		
		if (!isRegexValid) {
			throw new IllegalArgumentException(
					"Position or Direction values do not match the regular expression validation"); 
		}
		
		int positionXIndex = 0;
		int positionYIndex = 1;
		int directionIndex = 2;
		
		String[] positionAndDirection = positionAndDirectionToParse.split(" ");
		
		Position position = new Position(new Long(positionAndDirection[positionXIndex])
				, new Long(positionAndDirection[positionYIndex]));
		
		String cardinalId = (positionAndDirection[directionIndex] + "").toUpperCase();
		Direction direction = new Direction(CardinalDirection.valueOf(cardinalId));
		
		this.position = position;
		this.direction = direction;
	}
	
	public Location(Position position, Direction direction) {
		
		if (position != null && direction != null) {
			this.position = position;
			this.direction = direction;	
		
		} else {
			throw new IllegalArgumentException("Position or Direction values are null");
		}
	}
	
	public void turnLeft() {
		saveHistory();
		direction.left();
	}
	
	public void turnRight() {
		saveHistory();
		direction.right();
	}
	
	public void move() {
		saveHistory();
		position.move(direction);
	}
	
	private void saveHistory() {
		this.locationHistory.add(this.toString());
	}
	
	public List<String> getLocationHistory() {
		return new LinkedList<String>(locationHistory);
	}
	
	public Position getCurrentPosition() {
		return position.copy();
	}
	
	public Direction getCurrentDirection() {
		return direction.copy();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(position.toString());
		sb.append(" ");
		sb.append(direction.toString());
		return sb.toString();
	}
	
}
