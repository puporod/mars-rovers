package com.points.marsrovers.navigation;

import com.points.marsrovers.exploration.Camera;

public interface Engine extends Camera {
	void left();
	void right();
	void move();
	Location current();
}
