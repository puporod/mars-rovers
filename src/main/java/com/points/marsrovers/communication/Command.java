package com.points.marsrovers.communication;

public interface Command<T> {
	public void execute(T target);
}
