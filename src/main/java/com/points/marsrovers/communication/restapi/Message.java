package com.points.marsrovers.communication.restapi;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.List;
import com.points.marsrovers.navigation.Position;

public class Message implements Serializable {

	private static final long serialVersionUID = -4401018100058179681L;

	@NotNull(message = "Plateau cannot be empty")
	@Pattern(regexp = Position.POSITION_REGEX_PATTERN)
	private String plateau;

	@NotNull(message = "Rovers cannot be empty")
	@Valid
	private List<RoverInput> rovers;

	public String getPlateau() {
		return plateau;
	}

	public void setPlateau(String plateau) {
		this.plateau = plateau;
	}

	public List<RoverInput> getRovers() {
		return rovers;
	}

	public void setRovers(List<RoverInput> rovers) {
		this.rovers = rovers;
	}
}