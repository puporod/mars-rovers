package com.points.marsrovers.communication.restapi;

import java.io.Serializable;

public class RoverOutput implements Serializable {

	private static final long serialVersionUID = 6216633198860021208L;

	private String id;
	private String location;
	private String message;
	
	public RoverOutput(String id, String location, String message) {
		setId(id);
		setLocation(location);
		setMessage(message);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
