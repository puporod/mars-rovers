package com.points.marsrovers.communication.restapi;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.points.marsrovers.PlateauService;
import com.points.marsrovers.RoverService;
import com.points.marsrovers.navigation.Plateau;

@RestController
@RequestMapping(path = "/rover", produces = "application/json")
@CrossOrigin(origins = "*")
public class ApiController {

	@Autowired
	private PlateauService plateauService;
	
    @Autowired
    private RoverService roverService;

    @GetMapping(path = "/spirit")
    public ResponseEntity<List<RoverOutput>> getRovers() {
    	
        List<RoverOutput> rovers = roverService.listDeployedRovers();
        
        if (rovers != null && !rovers.isEmpty()) {
            return new ResponseEntity<>(rovers, HttpStatus.OK);
            
        } else {
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(path = "/spirit")
    public void deployRovers(@RequestBody @Valid Message message) {

        Plateau plateau = plateauService.createPlateuFrom(message.getPlateau());
        
        roverService.deployRovers(message.getRovers(), plateau);
        roverService.runRovers();
    }

}
