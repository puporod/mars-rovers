package com.points.marsrovers.communication.restapi;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.points.marsrovers.communication.EngineCommand;
import com.points.marsrovers.navigation.Location;

public class RoverInput implements Serializable {

	private static final long serialVersionUID = 499162690015053724L;

	@NotNull(message = "Location cannot be empty")
	@Pattern(regexp = Location.POSITION_DIRECTION_PATTERN)
	private String location;

	@NotNull(message = "Instruction cannot be empty")
	@Pattern(regexp = EngineCommand.REGEX_COMMAND_PATTERN)
	private String instruction;

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}
}