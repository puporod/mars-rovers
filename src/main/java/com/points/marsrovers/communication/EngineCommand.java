package com.points.marsrovers.communication;

import java.util.EnumMap;
import java.util.regex.Pattern;
import com.points.marsrovers.navigation.Engine;

public enum EngineCommand {
	
	L, R, M, P;
	
	public static final String REGEX_COMMAND_PATTERN = "(?i)^[LRM]{1,100}$";
	public static final Pattern REGEX_INPUT_VALIDATOR = Pattern.compile(REGEX_COMMAND_PATTERN);
	
	private static EnumMap<EngineCommand, Command<Engine>> availableCommands;
	static {
		availableCommands = new EnumMap<>(EngineCommand.class);
		availableCommands.put(EngineCommand.M, Engine::move);
		availableCommands.put(EngineCommand.L, Engine::left);
		availableCommands.put(EngineCommand.R, Engine::right);
	}
	
	public static Command<Engine> getCommandImplementation(String command) {
		EngineCommand cmd = EngineCommand.valueOf(command);
		if (availableCommands.containsKey(cmd)) {
			return availableCommands.get(cmd);
		}
		
		return (engine) -> { System.err.println("Command not implemented for EngineCommand"); };
	}
}
