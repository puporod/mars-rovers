package com.points.marsrovers.communication;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.points.marsrovers.navigation.Engine;

public class Instruction {
	
	private List<Command<Engine>> commandsToExecute;
	
	public Instruction(String instructions) {
		
		boolean areInstructionsValid = EngineCommand.REGEX_INPUT_VALIDATOR.matcher(instructions).matches();
		if (!areInstructionsValid) {
			throw new IllegalArgumentException("Instruction values do not match the regular expression validation");
		}
		
		commandsToExecute = new LinkedList<Command<Engine>>();
		Arrays.asList(instructions.split("")).forEach(cmd -> {
			commandsToExecute.add(EngineCommand.getCommandImplementation(cmd));
		});
	}
	
	public void executeOn(Engine engine) {
		commandsToExecute.forEach(cmd -> cmd.execute(engine));
		commandsToExecute.clear();
	}
}
