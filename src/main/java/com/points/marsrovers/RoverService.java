package com.points.marsrovers;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.points.marsrovers.communication.Instruction;
import com.points.marsrovers.communication.restapi.RoverInput;
import com.points.marsrovers.communication.restapi.RoverOutput;
import com.points.marsrovers.navigation.Location;
import com.points.marsrovers.navigation.Plateau;

@Service
public class RoverService {

    private List<Rover> rovers = new LinkedList<>();

    public List<Rover> deployRovers(List<RoverInput> roversInput, Plateau onPlateau) {

        roversInput.forEach(roverInput -> {
            Location location = new Location(roverInput.getLocation());
            Instruction instruction = new Instruction(roverInput.getInstruction());
           
            Rover spirit = new Spirit(location, instruction, onPlateau);
            rovers.add(spirit);
        });

        return rovers;
    }

    public List<RoverOutput> listDeployedRovers() {
    	
    	List<RoverOutput> output = new LinkedList<>();
    	
    	rovers.forEach(rover -> {
    		RoverOutput out = new RoverOutput(
    				rover.getId().toString()
    				, rover.current().toString()
    				, "Rover deployed successfully");
    		
    		if (rover.isDeployedOutsidePlateauBounds()) {
    			out.setLocation("UNKNOWN");
    			out.setMessage("Rover is lost! it was deployed out of Plateau's bounds!");
    		}
    		
    		output.add(out);
    	});
    	
        return output;
    }
    
    public void runRovers() {
    	rovers.forEach(rover -> rover.start());
    }
}

