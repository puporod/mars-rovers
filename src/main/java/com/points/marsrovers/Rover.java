package com.points.marsrovers;

import java.util.UUID;
import com.points.marsrovers.navigation.Engine;

public interface Rover extends Engine {
	UUID getId();
	void start();
	boolean isDeployedOutsidePlateauBounds();
}
