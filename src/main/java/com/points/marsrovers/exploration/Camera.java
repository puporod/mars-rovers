package com.points.marsrovers.exploration;

public interface Camera {
	void snapshot();
}
