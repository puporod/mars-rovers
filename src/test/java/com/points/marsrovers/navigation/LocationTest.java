package com.points.marsrovers.navigation;

import static org.junit.Assert.*;

import org.junit.Test;
import java.util.List;

public class LocationTest {
	
	@Test
	public void testConstructorString() {
		Location location = new Location("4 898989898 n");
		assertEquals("4 898989898 N", location.toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringYException() {
		new Location("4 ? N");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringXException() {
		new Location("# 19 W");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringDirectionException() {
		new Location("1 9 K");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringRegexException() {
		new Location("98anb7 abjhefg8732 biuabou3");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorDirectionNullValue() {
		new Location(new Position("2 4"), null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorPositionNullValue() {
		new Location(null, new Direction(CardinalDirection.S));
	}
	
	@Test
	public void testTurnLeft() {
		Position position = new Position(2L, 2L);
		Direction direction = new Direction(CardinalDirection.N);
		Location location = new Location(position, direction);
		
		location.turnLeft();
		assertEquals("2 2 W", location.toString());
		
		location.turnLeft();
		assertEquals("2 2 S", location.toString());
		
		location.turnLeft();
		assertEquals("2 2 E", location.toString());
		
		location.turnLeft();
		assertEquals("2 2 N", location.toString());
	}
	
	@Test
	public void testTurnRight() {
		Position position = new Position(2L, 2L);
		Direction direction = new Direction(CardinalDirection.N);
		Location location = new Location(position, direction);
		
		location.turnRight();
		assertEquals("2 2 E", location.toString());
		
		location.turnRight();
		assertEquals("2 2 S", location.toString());
		
		location.turnRight();
		assertEquals("2 2 W", location.toString());
		
		location.turnRight();
		assertEquals("2 2 N", location.toString());
	}
	
	@Test
	public void testMoveAndHistory() {

		Position position = new Position(2L, 2L);
		Direction direction = new Direction(CardinalDirection.N);
		Location location = new Location(position, direction);
		// 2 2 N
		location.move();
		assertEquals("2 3 N", location.toString());
		
		location.turnRight(); // 2 3 E
		location.move();
		assertEquals("3 3 E", location.toString());
		
		location.turnLeft(); // 3 3 N
		location.move(); // 3 4 N
		location.move(); // 3 5 N
		assertEquals("3 5 N", location.toString());
		
		List<String> history = location.getLocationHistory();
		StringBuilder sb = new StringBuilder();
		history.forEach(loc -> sb.append(loc + ", "));
		assertEquals("2 2 N, 2 3 N, 2 3 E, 3 3 E, 3 3 N, 3 4 N, ", sb.toString());
		assertEquals("3 5 N", location.toString());
		
	}
	
}
