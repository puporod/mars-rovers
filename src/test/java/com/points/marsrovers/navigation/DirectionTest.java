package com.points.marsrovers.navigation;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DirectionTest {
	
	@Test
	public void testLeft() {
		Direction direction = new Direction(CardinalDirection.N);
		direction.left();
		assertEquals(CardinalDirection.W, direction.get());
		
		direction.left();
		assertEquals(CardinalDirection.S, direction.get());
		
		direction.left();
		assertEquals(CardinalDirection.E, direction.get());	
	}
	
	@Test
	public void testRight() {
		Direction direction = new Direction(CardinalDirection.N);
		direction.right();
		assertEquals(CardinalDirection.E, direction.get());
		
		direction.right();
		assertEquals(CardinalDirection.S, direction.get());
		
		direction.right();
		assertEquals(CardinalDirection.W, direction.get());	
	}
	
	@Test
	public void testToString() {
		Direction direction = new Direction(CardinalDirection.N);
		assertEquals("N", direction.toString());
		direction.right();
		assertEquals("E", direction.toString());
		direction.right();
		assertEquals("S", direction.toString());
		direction.right();
		assertEquals("W", direction.toString());
	}

}
