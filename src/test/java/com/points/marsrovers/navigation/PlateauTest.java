package com.points.marsrovers.navigation;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlateauTest {
	
	@Test
	public void testConstructorString() {
		Plateau plateau = new Plateau("12 999999999");
		assertEquals("0 0 x 12 999999999", plateau.toString());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testPlateauMaxException() {
		new Plateau("1 9999999999");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringXException() {
		new Plateau("u17 2");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorStringYException() {
		new Plateau("23 afhe-");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testWrongPlateauUpperRightValue() {
		new Plateau(new Position(-100L, -200L));
	}
	
	@Test
	public void testPinPositionOutBounds() {
		Plateau plateau = new Plateau(new Position(5L, 5L));
		Position outLeftBottom = new Position(-1L, 0L);
		Position outUpperRight = new Position(5L, 6L);
		
		assertFalse(plateau.isPositionInsideBounds(outLeftBottom));
		assertFalse(plateau.isPositionInsideBounds(outUpperRight));
	}
	
	@Test
	public void testPinPositionInsideBounds() {
		Plateau plateau = new Plateau(new Position(5L, 5L));
		Position p1 = new Position(0L, 0L);
		Position p2 = new Position(5L, 5L);
		
		assertTrue(plateau.isPositionInsideBounds(p1));
		assertTrue(plateau.isPositionInsideBounds(p2));
	}
	
	@Test
	public void testPositionNotPinnedDueToColision() {
		Plateau plateau = new Plateau(new Position(5L, 5L));
		Position p1 = new Position(2L, 3L);
		Position p2 = new Position(2L, 3L);
		
		plateau.pin(p1);
		assertFalse(plateau.pin(p2));
	}

}
