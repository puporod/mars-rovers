package com.points.marsrovers.navigation;

import static org.junit.Assert.*;

import org.junit.*;

public class PositionTest {
	
	@Test
	public void testPositionToString() {
		Position p = new Position(4L, 9L);
		assertEquals("4 9", p.toString());
	}
	
	@Test
	public void testEqualHigherThan() {
		Position p1 = new Position(0L, 0L);
		Position p2 = new Position(1L, 1L);
		Position p3 = new Position(-1L, 0L);
		
		assertTrue(p1.equalHigherThan(p3));
		assertTrue(p2.equalHigherThan(p3));
		assertTrue(p2.equalHigherThan(p1));
	}
	
	@Test
	public void testEqualLowerThan() {
		Position p1 = new Position(0L, 0L);
		Position p2 = new Position(1L, 1L);
		Position p3 = new Position(-1L, 0L);
		
		assertTrue(p3.equalLowerThan(p1));
		assertTrue(p1.equalLowerThan(p2));
		assertFalse(p2.equalLowerThan(p1));
	}
	
	@Test
	public void testEqualHigherLowerNull() {
		Position p = new Position(0L, 0L);
		assertFalse(p.equalHigherThan(null));
		assertFalse(p.equalLowerThan(null));
	}
	
	@Test
	public void testEqualsPosition() {
		Position p1 = new Position("2 5");
		Position p2 = new Position(2L, 5L);
		
		assertTrue(p1.equals(p2));
		assertFalse(p1.equals(null));
		assertTrue(p1.equals(p1));
		assertFalse(p1.equals(new Long(5L)));
	}
}
