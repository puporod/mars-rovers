package com.points.marsrovers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.points.marsrovers.navigation.Plateau;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MarsRoversApplication.class)
public class PlateauServiceTest {
	
	@Autowired
	private PlateauService service;
	
	@Test
	public void testCreatePlateau() {
		Plateau plateau = service.createPlateuFrom("5 5");
		assertEquals("0 0 x 5 5", plateau.toString());
	}

}
