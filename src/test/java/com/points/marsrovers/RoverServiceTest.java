package com.points.marsrovers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.points.marsrovers.communication.restapi.RoverInput;
import com.points.marsrovers.communication.restapi.RoverOutput;
import com.points.marsrovers.navigation.Plateau;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MarsRoversApplication.class)
public class RoverServiceTest {

    private static Plateau plateau;
    private static List<RoverInput> roverInputs;

    @Autowired
    private RoverService service;

    @Before
    public void setUp() throws Exception {
        plateau = new Plateau("5 5");

        RoverInput spirit1 = new RoverInput();
        spirit1.setLocation("1 2 N");
        spirit1.setInstruction("LMLMLMLMM");

        RoverInput spirit2 = new RoverInput();
        spirit2.setLocation("3 3 E");
        spirit2.setInstruction("MMRMMRMRRM");

        RoverInput spiritOutOfBounds = new RoverInput();
        spiritOutOfBounds.setLocation("100 4 W");
        spiritOutOfBounds.setInstruction("MMRMMRMRRM");
        
        roverInputs = new LinkedList<>();
        roverInputs.add(spirit1);
        roverInputs.add(spirit2);
        roverInputs.add(spiritOutOfBounds);
    }

    @Test
    public void testDeployRovers() {

        List<Rover> rovers = service.deployRovers(roverInputs, plateau);

        assertNotNull(rovers);
        assertEquals(3, rovers.size());
        assertEquals("1 2 N", rovers.get(0).current().toString());
        assertEquals("3 3 E", rovers.get(1).current().toString());

        List<RoverOutput> roversOut = service.listDeployedRovers();

        assertNotNull(roversOut);
        assertEquals(3, roversOut.size());
        assertEquals("1 2 N", roversOut.get(0).getLocation());
        assertEquals("3 3 E", roversOut.get(1).getLocation());
        
        service.runRovers();
        roversOut = service.listDeployedRovers();
        
        assertEquals("1 3 N", roversOut.get(0).getLocation());
        assertEquals("5 1 E", roversOut.get(1).getLocation());
        assertEquals("UNKNOWN", roversOut.get(2).getLocation());
        
    }
}