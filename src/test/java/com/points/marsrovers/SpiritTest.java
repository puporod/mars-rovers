package com.points.marsrovers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.points.marsrovers.communication.Instruction;
import com.points.marsrovers.navigation.CardinalDirection;
import com.points.marsrovers.navigation.Direction;
import com.points.marsrovers.navigation.Location;
import com.points.marsrovers.navigation.Plateau;
import com.points.marsrovers.navigation.Position;

public class SpiritTest {

	@Test
	public void testRoverOne() {
		Plateau plateau = new Plateau(new Position(5L, 5L));
		Location location = new Location(new Position(1L, 2L), new Direction(CardinalDirection.N));
		Instruction instruction = new Instruction("LMLMLMLMM");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("1 3 N", spirit.current().toString());
	}

	@Test
	public void testRoverTwo() {
		Plateau plateau = new Plateau(new Position(5L, 5L));
		Location location = new Location(new Position(3L, 3L), new Direction(CardinalDirection.E));
		Instruction instruction = new Instruction("MMRMMRMRRM");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("5 1 E", spirit.current().toString());
	}
	
	@Test
	public void testRoverOneWithParserConstructor() {
		Plateau plateau = new Plateau("5 5");
		Location location = new Location("1 2 N");
		Instruction instruction = new Instruction("LMLMLMLMM");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("1 3 N", spirit.current().toString());
	}

	@Test
	public void testRoverTwoWithParserConstructor() {
		Plateau plateau = new Plateau("5 5");
		Location location = new Location("3 3 E");
		Instruction instruction = new Instruction("MMRMMRMRRM");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("5 1 E", spirit.current().toString());
	}
	
	@Test
	public void testRoverDeployedOnHugePlateau() {
		Plateau plateau = new Plateau("999999999 999999999");
		Location location = new Location("300 429901 E");
		Instruction instruction = new Instruction("MMRMMRMRRM");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("302 429899 E", spirit.current().toString());
	}
	
	@Test
	public void testRoverDeployedOnHugePlateauAndMaxPositioned() {
		Plateau plateau = new Plateau("999999999 999999999");
		Location location = new Location("999999999 999999999 E");
		Instruction instruction = new Instruction("RRRR");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.start();
		
		assertEquals("999999999 999999999 E", spirit.current().toString());
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testRoverCallingSnapshotMethodNotImplemented() {
		
		Plateau plateau = new Plateau("2 2");
		Location location = new Location("1 1 W");
		Instruction instruction = new Instruction("R");
		
		Rover spirit = new Spirit(location, instruction, plateau);
		spirit.snapshot();
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullValueConstructorRoverNotCreated() {
		new Spirit(null, new Instruction("LLM"), new Plateau("100 100"));
	}

	@Test
	public void testRoverIsLostWhenDeployedOutOfPlateauBounds() {
		Plateau plateau = new Plateau("2 2");
		Location location = new Location("10 20 N");
		Instruction instruction = new Instruction("RMM");

		Rover spirit = new Spirit(location, instruction, plateau);
		assertTrue(spirit.isDeployedOutsidePlateauBounds());
	}
}
