package com.points.marsrovers.communication;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.points.marsrovers.navigation.Engine;

public class EngineCommandTest {
	
	@Test
	public void testSelectedCommand() {
		assertNotNull(EngineCommand.getCommandImplementation("L"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSelectedCommandException() {
		EngineCommand.getCommandImplementation("i3u8");
	}
	
	@Test
	public void testSelectedCommandNotImplemented() {
		Command<Engine> cmd = EngineCommand.getCommandImplementation("P");
		cmd.execute(null);
		assertNotNull(cmd);
	}

}
