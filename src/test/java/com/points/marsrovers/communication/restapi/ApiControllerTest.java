package com.points.marsrovers.communication.restapi;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.points.marsrovers.MarsRoversApplication;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = MarsRoversApplication.class)
@WebAppConfiguration
public class ApiControllerTest {

    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
    	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
     }

    @Test
    public void testGetEmptyRovers() throws Exception {
        mockMvc.perform(get("/rover/spirit"))
                .andExpect(status().isNoContent());
    }
    
    @Test
    public void testDeployRovers() throws Exception {
    	
    	mockMvc.perform(post("/rover/spirit")
    			.contentType(MediaType.APPLICATION_JSON)
    			.content("{\"plateau\":\"5 5\", \"rovers\": [{\"location\":\"1 2 N\", \"instruction\":\"LMLMLMLMM\"}"
    					+ ",{\"location\":\"3 3 E\", \"instruction\":\"MMRMMRMRRM\"}]}"))
    	.andExpect(status().isOk());
    	
        mockMvc.perform(get("/rover/spirit"))
        .andExpect(status().isOk());
    }
}