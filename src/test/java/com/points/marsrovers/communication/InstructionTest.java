package com.points.marsrovers.communication;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class InstructionTest {
	
	@Test
	public void testConstructor() {
		Instruction instruction = new Instruction("LRMLRMMRL");
		assertNotNull(instruction);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testConstructorException() {
		new Instruction("LR ML 09i23j RMMRL");
	}
}
