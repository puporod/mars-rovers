# Mars Rovers Project

- [Problem Context](#problem-context)
- [Solution Idea](#solution-idea)
- [Architecture and Project Skeleton](#architecture-and-project-skeleton)
- [Build, Test and Run](#build-test-and-run)

## Problem Context
A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on-board cameras can get a complete view of the surrounding terrain to send back to Earth.

A rover's position and location is represented by a combination of x and y coordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example position might be 0, 0, N, which means the rover is in the bottom
left corner and facing North.

In order to control a rover, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the rover spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading.

Assume that the square directly North from (x, y) is (x, y+1).

Write a program that takes in instructions for the rovers’ movements and prints out their final positions.

INPUT:
The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0.

The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover's position, and the second line is a series of instructions telling
the rover how to explore the plateau.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y coordinates and the rover's orientation.

Each rover will be finished sequentially, which means that the second rover won't start to move until the first one has finished moving.

OUTPUT
The output for each rover should be its final coordinates and heading.

INPUT AND OUTPUT

Test Input:
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM

Expected Output:
1 3 N
5 1 E

## Solution Idea

The project allows a Rover to be deployed in a Plateau environment. The Rover, after deployed, will not make moves to the outside the Plateau's bounds or to a position that would colid with another deployed Rover in the same Plateau. Yet, a Rover might get lost if its Position, when first landing, is set to the outside the Plateau's bounds.

The Plateau class is responsible to pin/unpin, check for bounds and collisions among the Positions it manages.

The Position class manages its X and Y values and also moves itself to the proper direction based on a cardinal point (North, East, South, West).

The Direction class is responsible to manage the cardinal point of a Location, making use of the CardinalDirection enum.

The Location class has a Position and Direction and also keeps track of the previous locations of a Rover.

Regular Expressions are used to validate the Instructions and Commands inputted.

IllegalArgumentException is thrown when wrong input values are set.

Spirit is an implementation of a Rover. Opportunity and Curiosity were not implemented!

The Instruction class receives all the Commands of a Rover, validates the input and has the method *executeOn* to trigger the commands execution using the Engine interface.

The Rover interface extends the Engine which declares the movement behaviours to be implemented in a Rover.

### Architecture and Project Skeleton

 - The project uses Maven and Spring Boot as it is a robust and easy Java framework to work with
 - The package structure is divided by features instead of layers. The main packages are:
 	- communication
 	- exploration
 	- navigation
 - The main model objects are as follow:
 	- Rover: A vehicle designed to run on Mars eg. Spirit, Opportunity, Curiosity
 	- Plateau: The spacial grid where a rover will be deployed to explore
 	- Command: Part of an instruction set to be executed by the rover
 	- Instruction: A set of commands sent to a rover
 	- Location: The position and direction the rover is set
 - Other important objects are:
 	- Camera and Engine: Interfaces modules that are part of a rover

### Build, Test and Run

The stable version can be found under the tags [v1.2](https://gitlab.com/puporod/mars-rovers/tree/v1.2).

#### Steps to build the project:
 - First download or checkout the source code [v1.2](https://gitlab.com/puporod/mars-rovers/tree/v1.2)
 - Change directory to the project folder. eg:
    ```
    $ cd mars-rovers-v1.2/
    ```
 - Execute the maven wrapper command:
    ```
    $ ./mvnw clean install package
    ```

#### Test:
 - Execute the tests with:
    ```
    $ ./mvnw test
    ```

 The class **com.points.marsrovers.SpiritTest** has the main application test that also covers the Plateau, Location, Instruction and Rover classes. Individual test classes are also available testing the whole codebase.

#### Run:
A simple REST API is exposed in the project and can be tested by running the generated jar file inside *target/* and executing a shell script with the sample *data-post.json* file data.

 - Run the project jar 
   ```
    $ java -jar target/marsrovers-1.2.jar   
   ```
 - The application will be running on *http://localhost:8080/*
 - The simple script can be found inside the *script/* folder and can be executed under an unix environment with python installed.
 - The script executes a POST and then a GET to the application REST API with the sample data found on *script/data-post.json*:
    ```
    $ cd ./script && ./post-get-script.sh
    ``` 

Following are a simple POST and a GET request:
 
 - A POST request can be sent to the url *http://localhost:8080/rover/spirit* with the content body as a *"application/json" content-type*
    ```
    {  
       "plateau":"5 5",
       "rovers":[  
          {  
             "location":"1 2 N",
             "instruction":"LMLMLMLMM"
          },
          {  
             "location":"3 3 E",
             "instruction":"MMRMMRMRRM"
          },
          {  
             "location":"30 3 E",
             "instruction":"MMRMMRMRRM"
          }
       ]
    }
    ```
 - A GET resquest can be sent to the url *http://localhost:8080/rover/spirit* to receive a list of deployed/not deployed rovers. eg.:
    ```
    [
      {
        "id": "00db6b9d-1a0e-4cb4-be3e-05262efab64a",
        "location": "1 3 N",
        "message": "Rover deployed successfully"
      },
      {
        "id": "0ea83c26-0023-4e1a-8aac-37872e7be80a",
        "location": "5 1 E",
        "message": "Rover deployed successfully"
      },
      {
        "id": "5821c2f6-9846-4c48-b20a-d98c72b20ea6",
        "location": "UNKNOWN",
        "message": "Rover is lost! it was deployed out of Plateau's bounds!"
      }
    ]
    ```
