#!/bin/sh

HOST="http://localhost:8080/"
ROVER_URL="$HOST/rover/spirit"
GET_OUTPUT_FILE="data-get-response.json"

echo " Executing POST to $ROVER_URL"
curl -X POST -H 'Content-Type: application/json' -d '@data-post.json' $ROVER_URL

echo " Executing GET to $ROVER_URL"
curl -s $ROVER_URL > $GET_OUTPUT_FILE

echo " Data received: "
python -m json.tool < $GET_OUTPUT_FILE
